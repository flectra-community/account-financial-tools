# Copyright (C) 2020 Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "No Default Account",
    "version": "2.0.1.0.0",
    "development_status": "Beta",
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "maintainers": ["dreispt"],
    "summary": "Remove default expense account for vendor bills journal",
    "website": "https://gitlab.com/flectra-community/account-financial-tools",
    "license": "AGPL-3",
    "depends": ["account"],
    "category": "Accounting/Accounting",
    "data": [
        "views/account_journal_views.xml",
    ],
    "installable": True,
}
