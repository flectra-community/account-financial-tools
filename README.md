# Flectra Community / account-financial-tools

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_balance_line](account_balance_line/) | 2.0.1.0.0| Display balance totals in move line view
[account_lock_to_date](account_lock_to_date/) | 2.0.1.0.0|         Allows to set an account lock date in the future.
[account_fiscal_position_vat_check](account_fiscal_position_vat_check/) | 2.0.1.1.0| Check VAT on invoice validation
[account_move_name_sequence](account_move_name_sequence/) | 2.0.1.2.2| Generate journal entry number from sequence
[account_move_fiscal_year](account_move_fiscal_year/) | 2.0.1.0.1|         Display the fiscal year on journal entries/item
[account_invoice_constraint_chronology](account_invoice_constraint_chronology/) | 2.0.1.0.0| Account Invoice Constraint Chronology
[account_move_line_tax_editable](account_move_line_tax_editable/) | 2.0.2.0.0| Allows to edit taxes on non-posted account move lines
[product_category_tax](product_category_tax/) | 2.0.1.1.0| Configure taxes in the product category
[account_sequence_option](account_sequence_option/) | 2.0.1.0.2| Manage sequence options for account.move, i.e., invoice, bill, entry
[account_asset_batch_compute](account_asset_batch_compute/) | 2.0.1.0.0|         Add the possibility to compute assets in batch
[account_netting](account_netting/) | 2.0.1.0.0| Compensate AR/AP accounts from the same partner
[account_journal_general_sequence](account_journal_general_sequence/) | 2.0.1.0.2| Add configurable sequence to account moves, per journal
[account_move_force_removal](account_move_force_removal/) | 2.0.1.0.0| Allow force removal account moves
[stock_account_prepare_anglo_saxon_out_lines_hook](stock_account_prepare_anglo_saxon_out_lines_hook/) | 2.0.1.0.4| Modify when and how anglo saxon journal items are created
[account_asset_management_menu](account_asset_management_menu/) | 2.0.1.0.0| Assets Management Menu
[account_move_budget](account_move_budget/) | 2.0.1.0.0| Create Accounting Budgets
[account_check_deposit](account_check_deposit/) | 2.0.1.0.1| Manage deposit of checks to the bank
[account_no_default](account_no_default/) | 2.0.1.0.0| Remove default expense account for vendor bills journal
[account_move_template](account_move_template/) | 2.0.1.1.1| Templates for recurring Journal Entries
[account_chart_update](account_chart_update/) | 2.0.2.0.3| Wizard to update a company's account chart from a template
[account_move_line_menu](account_move_line_menu/) | 2.0.1.0.0| Adds a Journal Items menu
[account_lock_date_update](account_lock_date_update/) | 2.0.2.0.0|         Allow an Account adviser to update locking date without having        access to all technical settings
[account_asset_transfer](account_asset_transfer/) | 2.0.1.0.1| Asset Transfer from AUC to Asset
[account_menu](account_menu/) | 2.0.1.1.0| Adds missing menu entries for Account module
[account_move_fiscal_month](account_move_fiscal_month/) | 2.0.1.0.0| Display the fiscal month on journal entries/item
[account_move_line_sale_info](account_move_line_sale_info/) | 2.0.1.0.1| Introduces the purchase order line to the journal items
[account_template_active](account_template_active/) | 2.0.1.0.0| Allow to disable / enable account template items (tax, fiscal position, account)
[account_move_line_purchase_info](account_move_line_purchase_info/) | 2.0.1.0.2| Introduces the purchase order line to the journal items
[account_move_print](account_move_print/) | 2.0.1.0.0| Adds the option to print Journal Entries
[account_asset_management](account_asset_management/) | 2.0.2.7.2| Assets Management
[base_vat_optional_vies](base_vat_optional_vies/) | 2.0.1.0.3| Optional validation of VAT via VIES
[account_journal_lock_date](account_journal_lock_date/) | 2.0.1.0.1| Lock each journal independently
[account_cash_deposit](account_cash_deposit/) | 2.0.1.0.0| Manage cash deposits and cash orders
[account_loan](account_loan/) | 2.0.1.0.2| Account Loan management
[account_fiscal_month](account_fiscal_month/) | 2.0.1.0.0| Provide a fiscal month date range type
[account_fiscal_year](account_fiscal_year/) | 2.0.1.2.0| Create Account Fiscal Year
[account_asset_number](account_asset_number/) | 2.0.1.0.0| Assets Number


