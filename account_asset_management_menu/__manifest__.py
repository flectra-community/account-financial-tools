# Copyright 2021 ForgeFlow, S.L.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Assets Management Menu",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "depends": ["account_asset_management", "account_menu"],
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/account-financial-tools",
    "category": "Accounting & Finance",
    "data": ["views/menuitem.xml"],
    "auto_install": True,
}
